# Atmel

This repository contains a series of programs that initialize and demo various peripherals on several Atmel processors.  Programs are compiled using avr-gcc and flashed using Avrdude.  The programmer used to flash the processor is an Arduino board configured as ISP (See Examples->ArduinoISP in IDE).

Several of the projects have an eclipse_linux folder that contains the eclipse project.  I've had a difficult time generating a hex file in eclipse that matches that using the commandline build in the Makefile.  Therefore, I've been using the hex file generated in the commandline build, and eclipse as an assistent for formatting, auto-complete, etc. 

The following is a list of peripheral projects:

ATTiny85
--------

- blink: Flashes PB3 and PB4.  Uses timer to create the timebase for delay function.
- interrupt: A simple program that toggles PB3 and PB4 and captures interrupts on PB1 and PB2 (PCINT1, PCINT2).
- moore: A simple Moore FSM with several states that flash leds on PB3 and PB4.  User buttons on PB1 and PB2 control the state.  Button presses are captured on interrupts.  Uses a timer to create the timebase for the delay function.  See photo below for prototype (blue led is a bit obnoxious and makes one a bit quezzy to look at it).
- tempSensor: A continuation of the moore state machine program that uses the TMP36GZ (or similar) temperature sensor.  The state machine contains 4 states: Calibration, flash red / blue to show relative temperature, absolute temperature (F), and ADC reading.  See main.c for more details.  No schematic is included, but there should be enough text to reproduce the board.  This makes a great center piece on any coffee table :).  

Programming the ATTiny85
------------------------
The ATTiny85 can be programmed using an Arduino as the ISP.  There's a handful of writups how to do it, but here's the gist:
- Program the Arduino with the ISP sketch located in Examples -> ArduinoISP.  A few notes on the settings, I used the default ones but the baudrate is specific to the processor.  Use 19200 for the ATTiny85.
  - Baudrate 19200 (ATTiny85 does not use the same baud rate as the ATMega328p) - See Line 142 in the default sketch
  - #define SPI_CLOCK 		(1000000/6) - See Line 53 in the default sketch

- Connect the ATTiny85 to the Arduino using the following pinout:
- Arduino Pin - ATTiny85 Pin
    - Pin 10 to Pin 1 (Reset - No CS Pin Needed)
    - Pin 11 to Pin 5 (MOSI)
    - Pin 12 to Pin 6 (MISO)
    - Pin 13 to Pin 7 (SCK)
    - Pin 5v to Pin 8 (VCC)
    - Pin Gnd to Pin 4 (GND)

- Program the Attiny85 using either the Arduino IDE or commandline using AVRDude.  See Example projects for Makefile for how to do this.
